from django.urls import path

from api.views import get_j_coords, get_j_alerts
from .endpoint import history_sensor_handler

app_name = 'api'

endpointpatterns = [path('', history_sensor_handler, name='api'),
                    path('coords/', get_j_coords, name='coords'),
                    path('alerts/', get_j_alerts, name='alerts')]

urlpatterns = [] + endpointpatterns
