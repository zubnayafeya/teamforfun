from rest_framework.decorators import api_view, renderer_classes
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
import time, datetime

from sensors.models import History, Sensor, Statuses


@api_view(['POST'])
@renderer_classes((JSONRenderer,))
def history_sensor_handler(request):

    if request.method == 'POST':

        """
        'sensor',
        'status',
        'temp',
        'axilo',
        'sound',
        'bat',
        """

        sensor_id = request.query_params['sensor']
        sensor_status = request.query_params['status']
        print(sensor_status)
        sensor_temp = request.query_params['temp']
        sensor_axilo = request.query_params['axilo']
        sensor_sound = request.query_params['sound']
        sensor_bat = request.query_params['bat']
        print(f"{sensor_id} {sensor_status} {sensor_temp} {sensor_sound} {sensor_axilo} {sensor_bat}")

        try:
            Sensor.objects.get(id=sensor_id)
            print(f"Датчик найден {sensor_id} датчик")
        except BaseException as e:
            print(f"Ошибка: не найден датчик в БД - {str(e)}")
            pass
            return Response("Нет такого датчика в БД", status=404)

        sensor_id_base = Sensor.objects.get(id=sensor_id)
        sensor_status_base = Statuses.objects.get(id=sensor_status)

        history_event = History.objects.create(sensor=sensor_id_base, status=sensor_status_base, \
                                               axilo=sensor_axilo, sound=sensor_sound, bat=sensor_bat, temp=sensor_temp)
        history_event.save()

        return Response("Successful", status=200)
