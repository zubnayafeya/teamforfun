from django.shortcuts import render, get_list_or_404
from django.http import JsonResponse
from sensors.models import Sensor, History
# Create your views here.


def get_j_coords(request):
    try:
        coords = {x.sensorid: [x.coordinate_x, x.coordinate_y] for x in get_list_or_404(Sensor)}
    except Exception as e:
        print(e)
    return JsonResponse({'result': coords})


def get_j_alerts(request):
    try:
        # alerts = {x.sensor.sensorid: {'time': x.created,
        #                               'status': x.status.title} for x in get_list_or_404(History)}
        alerts = {}
        for sensor in get_list_or_404(Sensor):
            obj = History.objects.filter(sensor_id=sensor.id).latest('created')
            alerts[obj.sensor.sensorid] = {'time': obj.created,
                                 'status': obj.status.title}
        print(alerts)
    except Exception as e:
        print(e)
    return JsonResponse({'result': alerts})
