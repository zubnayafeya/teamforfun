from django.contrib import admin
from .models import Sensor, History, Statuses


@admin.register(Sensor)
class SensorAdmin(admin.ModelAdmin):
    list_display = [
        'sensorid',
        'coordinate_x',
        'coordinate_y',
        'created',
        'modified'
    ]


@admin.register(Statuses)
class StatusesAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'title',
        'snippet',
        'created',
        'modified'
    ]


@admin.register(History)
class HistoryAdmin(admin.ModelAdmin):
    list_display = [
        'sensor',
        'status',
        'temp',
        'axilo',
        'sound',
        'bat',
        'created',
        'modified'
    ]