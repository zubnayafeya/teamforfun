from django.db import models


class Statuses(models.Model):
    id = models.PositiveIntegerField(
        unique=True,
        primary_key=True,
        verbose_name='ID'
    )

    title = models.CharField(
        max_length=250,
        unique=True,
        verbose_name='название'
    )

    snippet = models.TextField(
        blank=True,
        null=True,
        verbose_name='описание'
    )

    modified = models.DateTimeField(
        auto_now=True,
        verbose_name='дата изменения'
    )

    created = models.DateTimeField(
        auto_now_add=True,
        verbose_name='дата создания'
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "статус"
        verbose_name_plural = "статусы"


class Sensor(models.Model):

    sensorid = models.CharField(
        max_length=250,
        unique=False,
        verbose_name='Ид сенсора'
    )

    coordinate_x = models.FloatField(
        blank=True,
        null=True,
        verbose_name='координата Х'
    )

    coordinate_y = models.FloatField(
        blank=True,
        null=True,
        verbose_name='координата Y'
    )

    modified = models.DateTimeField(
        auto_now=True,
        verbose_name='дата изменения'
    )

    created = models.DateTimeField(
        auto_now_add=True,
        verbose_name='дата создания'
    )

    def __str__(self):
        return self.sensorid

    class Meta:
        ordering = ['created']
        verbose_name = "датчик"
        verbose_name_plural = "датчики"


class History(models.Model):
    sensor = models.ForeignKey(
        Sensor,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        verbose_name='сенсор id'
    )

    status = models.ForeignKey(
        Statuses,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        verbose_name='статус'
    )

    axilo = models.CharField(
        max_length=25,
        blank=True,
        null=True,
        verbose_name='положение'
    )

    sound = models.CharField(
        max_length=25,
        blank=True,
        null=True,
        verbose_name='уровень шума'
    )

    temp = models.CharField(
        max_length=25,
        blank=True,
        null=True,
        verbose_name='температура'
    )

    bat = models.CharField(
        max_length=25,
        blank=True,
        null=True,
        verbose_name='уровень заряда'
    )

    modified = models.DateTimeField(
        auto_now=True,
        verbose_name='дата изменения'
    )

    created = models.DateTimeField(
        auto_now_add=True,
        verbose_name='дата создания'
    )

    # def __str__(self):
    #     return self.sensor

    class Meta:
        ordering = ['-created']
        verbose_name = "история"
        verbose_name_plural = "история"
