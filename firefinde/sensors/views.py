from django.shortcuts import render
from django.views.generic import ListView
from .models import History, Statuses


def home_controller(request):
    statuses = Statuses.objects.filter()
    context = {'text': 'maps yandex',
               'statuses': statuses
               }
    return render(request, 'sensors/home.html', context)


class HistoryList(ListView):
    model = History
    template_name = 'sensors/history_list.html'
    context_object_name = 'instance'
    paginate_by = 5
